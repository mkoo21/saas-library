## backend

- Activate the virtual env: `source ./venv/bin/activate`
- `pip3 install -r requirements.txt`
- The django app is under `library` - `cd library && python manage.py runserver`
- It should be listening on localhost port 8000

## frontend

- The frontend is under `ui` - `cd ui && yarn install --frozen-lockfile && yarn start`
- It should be listening on localhost port 1234

### Notes
- The searchbar at the top will filter books by title.
- The pages buttons will cycle through pages.
- The requirements weren't specific on what to do when the number of reservations for a book exceeds the quantity available. So I just made it not make a reservation/disable the button. There's a timestamp anyways so we could just save anyways and decide what to do with them later (e.g. it's there for when the book becomes available).
- I know the folder in the django app is called models but since there isn't a db these most definitely are not django models.
- The DBs are literally JSON files under `library/library` (yes I named the django app library as well. oops.) so to "reset" just delete them and replace the reservations file to have an empty list.
- It's not clear to me what "view books currently reserved" means, so I just had it display books that have a reservation.

