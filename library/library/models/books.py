import re
from .base_model import ModelMixin


class Books(ModelMixin):
    datasource = "./library/BooksDataSource.json"
    def find_by_title(title):
        print(title)
        pattern = ".*".join([c for c in title])
        print('pattern: {}'.format(pattern))
        data = Books._load_data()

        return list(
            filter(
                lambda book: bool(re.findall(pattern, book['title'].lower())),
                data))
