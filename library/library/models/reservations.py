from .base_model import ModelMixin
from .books import Books


class Reservations(ModelMixin):
    datasource = "./library/ReservationsDataSource.json"

    @staticmethod
    def get_reserved_books():
        reservation_data = Reservations._load_data()
        books_data = Books._load_data()
        reserved_ids = set()
        for reservation in reservation_data:
            reserved_ids.add(reservation['book_id'])
        return list(filter(
            lambda book: book['id'] in reserved_ids, books_data))

    @staticmethod
    def check_availability(book_id):
        reservations = len(Reservations.find_many({'book_id': int(book_id)}))
        # id should be unique but i didn't write find_one
        books = Books.find_many({'id': int(book_id)})
        print(reservations)
        if len(books):
            return books[0]['quantity'] > reservations
        return False
