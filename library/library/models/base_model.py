import json


class ModelMixin():
    @classmethod
    def _load_data(className):
        return json.loads(open(className.datasource, 'r').read())

    @classmethod
    def _write_data(className, data):
        return open(className.datasource, 'w').write(json.dumps(data))

    @classmethod
    def find_all(className):
        return className._load_data()

    @classmethod
    def find_many(className, query):
        print('Query: {}'.format(query))
        data = className._load_data()
        # Shallow match 
        return list(filter(lambda row: row.items() >= query.items(), data))

    @classmethod
    def insert_one(className, item):
        # does not check shape/schema
        data = className._load_data()
        data.append(item)
        className._write_data(data)
