from enum import Enum
from django.core.paginator import Paginator
from django.http import HttpResponseBadRequest
from django.http import JsonResponse
from django.utils import timezone
from rest_framework.views import APIView

from library.models import Books
from library.models import Reservations


PAGE_SIZE = 3

class LIBRARY_COMMANDS(Enum):
    BOOKS = 'BOOKS'
    RESERVE = 'RESERVE'
    RESERVED_BOOKS = 'RESERVED_BOOKS'


class LibraryView(APIView):
    def post(self, request):
        cmd = request.data.get('command')
        page = request.data.get('page')
        print('Handled command {}'.format(cmd))
        if cmd is None or LIBRARY_COMMANDS(cmd) == LIBRARY_COMMANDS.BOOKS:
            title = request.data.get('title')

            query = {}
            data = {}
            if title:
                data = Paginator(
                    Books.find_by_title(title),
                    PAGE_SIZE)
            else:
                data = Paginator(Books.find_many(query), PAGE_SIZE)

            page_data = data.page(page or 1).object_list
            # Tag books with 'available' or not
            for book in page_data:
                book['available'] = Reservations.check_availability(book['id'])

            return JsonResponse({
                'pages': data.num_pages,
                'books': page_data,
            })

        elif LIBRARY_COMMANDS(cmd) == LIBRARY_COMMANDS.RESERVE:
            username = request.data.get('username')
            book_id = request.data.get('book_id')
            if not Reservations.check_availability(book_id):
                # Refuse to reserve if all copies are reserved - maybe we want to log the reservation anyways?
                return JsonResponse({'success': False})

            reservation = {
                'book_id': int(book_id),
                'timestamp': str(timezone.now()),
                'username': username,
            }
            Reservations.insert_one(reservation)
            return JsonResponse({'success': True, **reservation})

        elif LIBRARY_COMMANDS(cmd) == LIBRARY_COMMANDS.RESERVED_BOOKS:
            data = Paginator(Reservations.get_reserved_books(), PAGE_SIZE)
            return JsonResponse({
                'pages': data.num_pages,
                'books': data.page(page or 1).object_list,
            })

        return HttpResponseBadRequest(
            '{} command not recognized.'.format(cmd))
