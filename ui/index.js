import React from 'react';
import ReactDOM from 'react-dom';
import { Books } from '/src/components/books';

const App = () => {
    return (
        <div>
            <h1> Welcome to SaaSville Public Library</h1>
            <Books />
        </div>
    )
};
ReactDOM.render(<App />, document.getElementById('root'));