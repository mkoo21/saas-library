import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import axios from 'axios';

import { API_URL } from '/src/env';

const Row = styled.div`
    display: flex;
    flex-direction: row;
`;
const Book = (props) => {
    const handleReserve = () => {
        const username = window.prompt('What is your name?')
        if(!username) return
        axios.post(API_URL, {
            command: "RESERVE",
            book_id: props.book.id,
            username,
        }).then(response => {
            if (!response.data.success) {
                alert('Sorry, there are currently no copies available')
            } else {
                props.fetchReservations();
                props.fetchBooks();
                alert(`Success! A copy of ${props.book.title} has been reserved for ${username}`);
            }
        });
    }
    return (<div>
        <h3>{props.book.title}</h3>
        <p>{props.book.author}</p>
        <p>Available: { props.book.available ? 'Yes' : 'No' }</p>
        <button onClick={handleReserve} disabled={!props.book.available}>Reserve</button>
    </div>)
};

const Reservations = (props) => {
    const handlePage = (pageNum) => () => props.setPage(pageNum);
    return (
        <>
            <h2>Books with reservations</h2>
            { props.reservations.map(x => <p>{x.title}</p>)}
            <p>Pages</p>
            <Row>{[ ...Array(props.pages).keys() ].map(x => <button key={x} onClick={handlePage(x + 1)}>{x + 1}</button>)}</Row>
        </>
    )
};

export const Books = () => {
    const [ books, setBooks ] = useState([]);
    const [ page, setPage ] = useState(1)
    const [ pages, setPages ] = useState(1);
    const [ reservations, setReservations ] = useState([]);
    const [ reservationPage, setReservationPage ] = useState(1);
    const [ reservationPages, setReservationPages ] = useState(1);
    const [ search, setSearch ] = useState({});

    useEffect(() => {
        fetchBooks();
    }, [page, search]);

    // useEffect(() => {
    //     axios.post(API_URL, { ...search, page }).then(resp => {
    //         setBooks(resp.data.books);
    //         setPages(resp.data.pages);
    //     });
    // }, [page, search]);

    useEffect(() => {
        fetchReservations()
    }, [reservationPage]);

    const fetchBooks = async () => {
        const resp = await axios.post(API_URL, { ...search, page });
        setBooks(resp.data.books);
        setPages(resp.data.pages);
    }
    const fetchReservations = async () => {
        const resp = await axios.post(API_URL, {
            command: "RESERVED_BOOKS",
            reservationPage,
        });
        setReservations(resp.data.books);
        setReservationPages(resp.data.pages);
        debugger;
        return resp;
    }

    const handlePage = (pageNum) => () => setPage(pageNum);
    const handleSearch = (changeEvent) => {
        // probably should debounce this but whatever
        setPage(1);
        const newSearch = changeEvent.target.value ? { title: changeEvent.target.value.toLowerCase() } : {};
        setSearch(newSearch)
    }
    return <div>
        <input type="text" placeholder="search" onChange={handleSearch} />
        <h2>Books</h2>
        { books.map(data => <Book book={data} key={data.id} fetchReservations={fetchReservations} fetchBooks={fetchBooks} />) }
        <p>Pages: </p>
        <Row>{[ ...Array(pages).keys() ].map(x => <button key={x} onClick={handlePage(x + 1)}>{x + 1}</button>)}</Row>
        <Reservations reservations={reservations} pages={reservationPages} setPage={setReservationPage} fetchReservations={fetchReservations} />
    </div>;
};
